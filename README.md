An architectural framework for Swift with MVVM-related types and relationships.

MVVMKit provides generic code to standardize implementations of the model-view-view-model architecture in Swift. It primarily focuses on the heirarchy from the underlying data to the presented view rather than the contents at each layer. By inheriting from its classes or conforming to its protocols, specialized types adhere to common conventions throughout a project. By understanding these conventions, many developers can contribute to a project in a highly consistent fashion. In short, MVVMKit strives to provide a solid foundation for maintainable projects that follow the model-view-view-model architecture.

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact